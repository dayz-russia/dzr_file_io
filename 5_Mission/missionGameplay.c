//TODO: Wrap Prints into enableDebug

modded class MissionGameplay
{
	ref map<string, string> m_DZR_ServerConfig;
	
	void MissionGameplay()
	{
		Print("[dzr_framework] ::: Starting Clientside");
		GetRPCManager().AddRPC( "DZR_FRM", "DZR_FileContentsToClient", this, SingleplayerExecutionType.Both );	
		GetRPCManager().AddRPC( "DZR_FRM", "DZR_OnPlayerConnect", this, SingleplayerExecutionType.Both );	
		GetRPCManager().AddRPC( "DZR_FRM", "DZR_AddToGlobalConfigOnClient", this, SingleplayerExecutionType.Both );	
		m_DZR_ServerConfig = new map<string, string>;
	}
	
	void DZR_AddToGlobalConfigOnClient(CallType type, ref ParamsReadContext ctx, ref PlayerIdentity sender, ref Object target)
	{
		
		Param2<string, string> data;
		if ( !ctx.Read( data ) ) return;
		
		if (type == CallType.Client)
		{
			Print("[dzr_framework] ::: DZR_AddToGlobalConfigOnClient ::: received global configs from server");
			GetGame().Chat("[dzr_framework] DZR_AddToGlobalConfigOnClient ::: data.param1 : "+data.param1+", data.param2 : "+data.param2, "colorImportant");
			Print(data.param1);
			Print(data.param2);
			GetDayZGame().DZRF_SaveClientCfg(data.param1,data.param2);
		}
	}
	
	void DZR_OnPlayerConnect(CallType type, ref ParamsReadContext ctx, ref PlayerIdentity sender, ref Object target)
	{
		
		Param2<string, string> data;
		if ( !ctx.Read( data ) ) return;
		
		if (type == CallType.Client)
		{
			GetGame().Chat("[dzr_framework] DZR_FileContentsToClient ::: data.param1 : "+data.param1+", data.param2 : "+data.param2, "colorImportant");
			GetDayZGame().DZRF_SaveClientCfg(data.param1,data.param2);
		}
		
		/*
			string DZR_SrvConfigDir = "$profile:DZR_DEL4/23423/324324/543534534/43543534";
			
			DZR_GetServerConfig(DZR_SrvConfigDir, "enableDebug.txt");
			DZR_GetServerConfig(DZR_SrvConfigDir, "123.txt");
			DZR_GetServerConfig(DZR_SrvConfigDir, "DEFAULTS.txt");
			DZR_GetServerConfig(DZR_SrvConfigDir, "Deletio.txt");
			Print("========== MissionGameplay");
			Print(m_DZR_ServerConfig);
		*/
	}
	
	void DZR_FileContentsToClient(CallType type, ref ParamsReadContext ctx, ref PlayerIdentity sender, ref Object target)
	{
		Param2<string, string> data;
		if (!ctx.Read(data))
		return;
		
		GetGame().Chat("[dzr_framework] DZR_FileContentsToClient ::: data.param1 : "+data.param1+", data.param2 : "+data.param2, "colorImportant");
		Print(data.param1);
		Print(data.param2);
		GetDayZGame().DZRF_SaveClientCfg(data.param1,data.param2);
		
		/*
			
			Print(params);
			string fileName = params.param1;
			string fileContents = params.param2;
			m_DZR_ServerConfig.Insert(fileName, fileContents);
			GetGame().Chat("Received from server: "+fileName+" = "+fileContents, "colorImportant");
			Print("========== DZR_FileContentsToClient");
			Print(m_DZR_ServerConfig);
		*/
	}
	
	void DZR_GetServerConfig(string dirName, string fileName)
	{
		PlayerBase player = PlayerBase.Cast( GetGame().GetPlayer() );
		PlayerIdentity sender = player.GetIdentity();
		ref Param2<string,string> m_Data = new Param2<string,string>(dirName, fileName);
		GetRPCManager().SendRPC( "DZR_FRM", "DZR_ReadFileOnServer", m_Data, true, sender);
	}
	
}