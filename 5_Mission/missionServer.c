modded class MissionServer
{
	void MissionServer()
	{
		Print("[dzr_framework] ::: Starting Serverside");
		string m_NameFile = "$profile:DZR_DEL4/23423/324324/543534534/43543534";
		string fileContent;
		/*
		fileContent = DZRF().GetFile(m_NameFile, "enableDebug.txt", "0");
		fileContent = DZRF().GetFile(m_NameFile, "123.txt", "1234");
		fileContent = DZRF().GetFile(m_NameFile, "DEFAULTS.txt");
		fileContent = DZRF().GetFile(m_NameFile, "actionName.txt", "ACTION NAME FROM FILE");
		fileContent = DZRF().GetFile(m_NameFile, "Deletio.txt", "", DZR_IO_Command.DELETE);
		//fileContent = DZRF().GetFile(m_NameFile, "DELETIO/", "", DZR_IO_Command.DELETE);
		Print(fileContent);
		//fileContent = DZRF().GetFile(m_NameFile, "123.txt", "857345 430543 05983459034 58", DZR_IO_Command.DELETE);
		//Print( "======== Deleted objects: "+DZR_DeleteFolder( m_NameFile ) ); 
		GetRPCManager().AddRPC( "DZR_FRM", "DZR_ReadFileOnServer", this, SingleplayerExecutionType.Both );	
		//GetFileNameWithoutExtension
		
		GetDayZGame().DZRF_AddToServerCfg("actionName","MissionServer ACTION NAME FROM FILE");
		*/
		GetDayZGame().DZRF_SetGlobalRoorDir("$profile:\\DZR\\CfgDemo2");
		GetDayZGame().DZRF_CreateConfig("actionName.txt","MissionServer ACTION NAME FROM FILE");
		GetDayZGame().DZRF_CreateConfig("actionName2.txt","2MissionServer ACTION NAME FROM FILE");
		GetDayZGame().DZRF_UpdateConfigsGlobally();
	}
	
	void DZR_ReadFileOnServer(CallType type, ref ParamsReadContext ctx, ref PlayerIdentity sender, ref Object target)
	{
		Param2<string, string> params;
		if (!ctx.Read(params))
		return;
		
		string dirName = params.param1;
		string fileName = params.param2;
		string fileNameWex;
		string path = dirName +"/"+ fileName;
		
		
		TStringArray array_name = new TStringArray;
		fileName.Split( ".", array_name );
		Print(array_name);
		int array_name_entries_count = array_name.Count();
		Print(array_name_entries_count);
		string fileContent = DZRF().GetFile(dirName, fileName);
		fileNameWex = array_name.Get(array_name_entries_count - 2);
		Print(fileNameWex);
		ref Param2<string,string> m_Data = new Param2<string,string>(fileNameWex, fileContent);
		GetRPCManager().SendRPC( "DZR_FRM", "DZR_FileContentsToClient", m_Data, true, sender);
		
	}
	
	
	
	int DZR_DeleteFolder(string m_DZRdir)
	{
		
		if(m_DZRdir == "$profile:DZR/") {return false;};
		if(m_DZRdir == "$profile:DZR") {return false;};
		
		string	file_name;
		int 	file_attr;
		int		flags;
		
		string fullPath = m_DZRdir;
		
		FindFileHandle file_handler = FindFile(fullPath+"/*", file_name, file_attr, flags);
		
		bool found = true;
		int file_counter = 0;
		int folder_counter = 0;
		bool noErrors = true;
		
		FileHandle fhandle;
		while ( found )
		{	
			if(file_attr != FileAttr.DIRECTORY)
			{
				noErrors = noErrors + DeleteFile( fullPath+"/"+file_name ); 
				file_counter++;
			} 
			else
			{
				folder_counter = folder_counter + DZR_DeleteFolder(fullPath+file_name+"/");
				DeleteFile( fullPath+"/"+file_name ); 
				folder_counter++;
			};
			found = FindNextFile(file_handler, file_name, file_attr);
		}
		DeleteFile( fullPath ); 
		Print ("[dzr_framework] ::: DELETING "+fullPath+" ------- Deleted files: "+file_counter+". Deleted folders: "+folder_counter );
		return file_counter + folder_counter;
	}
	
    override void InvokeOnConnect(PlayerBase player, PlayerIdentity identity)
    {
        super.InvokeOnConnect(player, identity);
		//GetRPCManager().SendRPC( "DZR_FRM", "DZR_OnPlayerConnect", NULL, true, identity);
		GetDayZGame().DZRF_UpdateConfigsGlobally();
		
	};
	

	
}	