#ifdef DZRF_demo
	
	modded class ActionConstructor
	{
		override void RegisterActions(TTypenameArray actions)
		{
			super.RegisterActions(actions);
			actions.Insert(ActionActivatePortal);
		}
	}
	
	modded class PlayerBase
	{
		override void SetActions()
		{
			super.SetActions();
			AddAction(ActionActivatePortal);
		}
	}
	
	
	class ActionActivatePortal: ActionInteractBase
	{
		//PortalTrigger m_PortalTrigger;
		string m_PortalActionName = "NAME FROM SCRIPT";
		string m_PortalName;
		bool m_UserAllowed;
		
		
		void ActionActivatePortal()
		{
			m_CommandUID = DayZPlayerConstants.CMD_ACTIONMOD_INTERACTONCE;
			m_StanceMask = DayZPlayerConstants.STANCEMASK_ERECT | DayZPlayerConstants.STANCEMASK_CROUCH;
			//GetRPCManager().AddRPC("dzr_portals", "SetPortalAttributesOnClient", this, SingleplayerExecutionType.Client);
			//GetRPCManager().AddRPC("dzr_portals", "SetUserAllowed", this, SingleplayerExecutionType.Client);
			
			Print("[dzr_framework] ::: Starting Serverside ActionActivatePortal");
			
		}
		
		/*
			if ( GetGame().IsMultiplayer() && GetGame().IsServer() )
			{
			if (GetGame().GetMission())
			{
			GetGame().GetMission().SyncRespawnModeInfo(GetIdentity());
			}
			}
		*/
		
		void SetPortalAttributesOnClient(CallType type, ref ParamsReadContext ctx, ref PlayerIdentity sender, ref Object target)
		{
			Param2<string,bool> params;
			if (!ctx.Read(params))
			return;
			
			m_PortalActionName = params.param1;
			m_UserAllowed = params.param2;
		}
		
		override void CreateConditionComponents() {
			m_ConditionItem = new CCINone;
			m_ConditionTarget = new CCTNone;
		}
		
		
		
		
		
		override string GetText()
		{
			//Print( GetDayZGame().DZRF_getCfg("globalCfg") );
			//Print( GetDayZGame().DZRF_getCfg("isDiagMPClient") );
			return GetDayZGame().DZRF_ReadConfig("actionName.txt");
			//return GetDayZGame().DZRF_getFileCfg("globalCfg");
			//return m_PortalActionName;
		}
		
		void SetText(string theActionName)
		{
			m_PortalActionName = theActionName;
			Print("Set Name for a portal: "+m_PortalActionName);
		}
		
		override bool ActionCondition( PlayerBase player, ActionTarget target, ItemBase item )
		{
			return true;
		}
		
		void DZR_sendPlayerMessage(PlayerBase player, string message)	
		{
			Param1<string> Msgparam;
			Msgparam = new Param1<string>(message);
			GetGame().RPCSingleParam(player, ERPCs.RPC_USER_ACTION_MESSAGE, Msgparam, true, player.GetIdentity());
		}
		override void OnStart( ActionData action_data )
		{
			super.OnStart( action_data );
			
			string m_NameFile = "$profile:DZR_DEL4/23423/324324/543534534/43543534";
			string fileContent;
			
			fileContent = DZRF().GetServerFile(m_NameFile, "actionFile.txt", "OnStart Server");
			Print("[dzr_framework] ActionActivatePortal ::: OnStart: GetServerFile"+fileContent);
			fileContent = DZRF().GetClientFile(m_NameFile, "actionFile.txt", "OnStart Client");
			Print("[dzr_framework] ActionActivatePortal ::: OnStart: GetClientFile"+fileContent);
			/*
			TStringArray ingredientsKeys = GetDayZGame().DZRF_ServerConfig.GetKeyArray();
			ingredientsKeys.Sort();

			//array<ref ExpansionBookCraftingItem> sortedIngredients = new array<ref ExpansionBookCraftingItem>;

			foreach (string ingredientsKey : ingredientsKeys)
			{
				Print("GetDayZGame().DZRF_ServerConfig.Get(ingredientsKey)");
				Print( GetDayZGame().DZRF_ServerConfig.Get(ingredientsKey) );
			}
			*/
			
		}
		
		override void OnStartServer( ActionData action_data )
		{
			super.OnStartServer(  action_data );
			
			string m_NameFile = "$profile:DZR_DEL4/23423/324324/543534534/43543534";
			string fileContent;
			
			fileContent = DZRF().GetServerFile(m_NameFile, "globalCfg.txt", "this is added after client action");
		}
		
	};
	
#endif